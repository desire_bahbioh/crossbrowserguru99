package TestSuite;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import org.testng.annotations.Test;


public class CrossBrowserTesting {


    @Test(groups = {"testing-firefox"})
    public void firefoxTest(){
        WebDriver f_driver = new FirefoxDriver();
        f_driver.get("http://demo.guru99.com/V4/");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement username = f_driver.findElement(By.name("uid"));
        username.clear();
        username.sendKeys("mngr38445");
        WebElement password = f_driver.findElement(By.name("password"));
        password.clear();
        password.sendKeys("ahusArY");
        f_driver.quit();
    }

    @Test(groups = {"testing-chrome"})
    public void chromeTest(){
        System.setProperty("webdriver.chrome.driver","./chromedriver");
        WebDriver c_driver = new ChromeDriver();
        c_driver.get("http://demo.guru99.com/V4/");
        WebDriverWait wait = new WebDriverWait(c_driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("uid")));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement username = c_driver.findElement(By.name("uid"));
        username.clear();
        username.sendKeys("mngr38445");
        WebElement password = c_driver.findElement(By.name("password"));
        password.clear();
        password.sendKeys("ahusArY");
        c_driver.quit();
    }

    @Test(groups = {"testing-safari"})
    public void safariTest(){
        WebDriver s_driver = new SafariDriver();
        s_driver.get("http://demo.guru99.com/V4/");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement username = s_driver.findElement(By.name("uid"));
        username.clear();
        username.sendKeys("mngr38445");
        WebElement password = s_driver.findElement(By.name("password"));
        password.clear();
        password.sendKeys("ahusArY");
        s_driver.quit();
    }


}
